# This is how I mounted juan's home of nodos, into juan's office machine:

:warning: I've replaced parts of the actually IPs with "<>" for security since this file is public in gitlab
make sure to properly fix them


## 1. Tell the nodos machine to start exporting juan's home in nodos:

In nodos, add this to **/etc/exports**

```
# don't forget to fix the IP
/home/ska/jtena   192.168.<>.<>/24(rw,async,no_subtree_check,all_squash,anonuid=<juans_nodos_ID>,anongid=<juans_nodos_ID>)
```


**all_squash**: everyone mounting this will be treated as the same 'anon' user      
**anonid,anongid**: the anonymous user gets given this user id and group id       
since we're mounting a folder of jtena, whose id and gid are XXX, we set the anonids to that       
       
## Make sure to restart the nfs service:

```bash
# in nodos, as root ofcourse:
service nfs-server restart

# check that it worked fine:
service nfs-server status
```

## 2. Get the office machine to mount the folder that we just started exporting

Make sure that the folder where we want to mount this exists:
```bash
sudo mkdir /mnt/nodos
```

in **/etc/fstab** add:

```
192.168.<>.<>:/home/ska/jtena /mnt/nodos nfs rw,async,nfsvers=3 0 0
```

To manually mount this the first time (otherwise, since we put this in fstab, it'll be automounted on startup)

```
sudo mount -a
```
