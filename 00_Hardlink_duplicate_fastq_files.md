# The problem

Our home has a lot of duplicated fastq.gz files. These are un-necessary and take up a lot of useful space.
Some files are duplicated multiple times.

# The solution

For each group of duplicated files, we we will select (following some rules, such as: juan's files are a priority) 
one 'original' (this is arbitrary, they are all the same) and replace all the other files with hardlinks to the original file.

# What is a hardlink?

Hardlinks are everywhere in a linux system. In fact every file is a hardlink.
A hardlink consists of two parts, a name or a path of the file and the underlying data.

For example, the file /home/file1.txt is a hardlink with the name '/home/panos/file1.txt' pointing to the underlying data.

```
/home/file1.txt ---> ((data1))
```

Renaming/moving a file does nothing to the underlying data, it just changes the name of the hardlink.



When we copy a file, we make a copy of its data too:
```
cp file1 file2:
/home/file1.txt ---> ((data1))
/home/file2.txt ---> ((data2)) #data2 is exactly the same as data1 but it's a separate copy
```

When we make a symbolic link, we create a second file (ie a second name/path) that points to the first path (insted of some data):

```
ln -s file1 file2symbolic means:

/home/file2symbolic --> /home/file1.txt --> ((data1))
```

If /home/file1 is renamed/moved the symbolic link breaks.



When we make a hardlink, we don't point to the name of the original file but to its actual data instead:

```
ln file1 file2hard means:
/home/file2hard --> ((data1))
```

Now if /home/file1.txt is moved, the file2hard will not be affected since it was pointing to file1's data, not file1's name. 


When we make a hardlink then,we have two or more different names/paths that are linked to said data. 
You can see this in 'ls':

```
-rw-rw-r-- 1 panos panos          9 Feb 26 16:13 passfile
-rw-r--r-- 2 root  root          11 Feb 27 12:56 testlink.txt
-rw-r--r-- 2 root  root          11 Feb 27 12:56 test.txt
```

The number right after the permisions and before the owner's name is how many times that data is linked.
In the example above, the two root-owned files are hardlinks to the same data, so they both have 2

### CAVEATS

A potential problem of having hardlinks (especially in a multi-user home) is that if we open the new hardlink and edit the data,
the original file is also changed.
In our case, since we'll only be hardlinking fq files (which are never edited), we should be safe.
Additionally, all created hardlinks will be read-only.


### Recap

To recap, we have many duplicated files:
```
/home/file1 --> ((data_zebra_1))
/home/file2 --> ((data_zebra_1_copy))
/home/file3 --> ((data_zebra_1_othercopy))
/home/file4 --> ((data_zebra_1_thirdcopy))

```

And we'll save space by removing the unecessary copies and replacing them with hardlinks:
```
/home/file1 --> ((data_zebra_1))
/home/file2 --> ((data_zebra_1))
/home/file3 --> ((data_zebra_1))
/home/file4 --> ((data_zebra_1))

```


# Finding the duplicated files:

I will be using this program which is designed to clean up filesystems:
https://rmlint.readthedocs.io/en/latest/

Duplicate files are found based on the checksum value, that's a very interesting topic for another time.

### Find fastq files
There are a few unzipped fq files in the system, I won't do anything for now but it's good to track them:

There's a script in /root/findfq to fascilitate.
I have also manually added some blacklisted folders where fq files are found but they are of no interest to us (example files for programs and so on)


### Find fastq.gz files
In /root/findfqgz is a small collection of scripts:

We find putative files (not links)
exclude some blacklisted folders
filter bad symlinks (rmlint should handle this but it has a bug)
```bash
find ${1} -type f ! -type l  -regex  '.*\(.fq.gz\|.fastq.gz\)$'  | grep -vFf excludes.txt | ./filter_bad_symlinks.sh(py3_venv) [root@nodos findfqgz]
```
What is output from this command are all fastq.gz files that we might want to backup.


We feed this to rmlint:

```bash
thepreviouscommand | \
rmlint -x --keep-hardlinked -t 8 \
-o summary:stdout -o pretty:stdout -o json:bigrmlintrun.json -o csv:bigrmlintrun.csv -o uniques:bigrmlintUniques.txt \
-c sh:hardlink -o sh:bigrmlintrun.sh \
-S d'r<^/home/ska/jtena/.*$>''R<^/home/ska/alvaro.*$>''R<^/home/ska/sjimgan.*$>'Oma
```

-x makes rmlint stay on the same partition       
--keep-hardlinked is important for the runs after the first one, it tells rmlint to not try and remove already existing hardlinks
the -o options setup how rmlint will output things, -c tells it that we're looking to make hardlinks of the duplicates, not remove them
the -S determines how rmlint decides which file is the original, here we've said that juan's files are a priority to be considered original
while alvaro's and sandra's will be considered last.


### RMLINT results

RMlint give us three important categories of files:

Unique files: These have no duplicates, they are only found once in our system. We need to keep those safe

Original files: These have duplicates. We need to keep them safe

Duplicated files: We will delete these and replace them with hardlinks to their 'original file'


### Backups

I backed-up all unique and original files, keeping their relative path into /mnt/raNAS/datavault/

```bash
$RSYNC                                                          \
        -v -n -PRW                              \
        --files-from=${FILE_LIST}                                              \
        / $BACKUPDIR
```

Check everything has passed to the backup:
```bash
rsync -vPR --size-only --files-from=tobebackedup.txt / admin@192.168.49.95:/share/CACHEDEV1_DATA/homes/datavault/ 2>&1 | tee last_backup_check.log
```

### RMlint copy hardlinks

RMLint doens't delete any file itself, instead it produces a bash script which we can investigate and change before we run it

For every original-duplicate file that it is going to handle, rmlint makes a number of checks to make sure things are still what we 
expect them to be.

To make it also check that the original file has been backedup before doing the deduplication, I added the following snippet in the 
rmlint.sh, 

inside the original_check() function:

```bash
    e_backup='/mnt/raNAS/datavault'"$2"
    if [ ! -e "$e_backup" ]; then
          echo "${COL_RED}^^^^^^ Error: Cannot find the original file backup - cancelling.....${COL_RESET}"
          echo $2 $e_backup
          return 1
    fi
```

We can now let rmlint loose, it'll take every pair of original-duplicate fastq.gz and them:
- make sure the original file is still there      
- the duplicate file is still there      
- the original file has been backed up      
- original and duplicate are NOT the exact same filename       
- original and duplicate are still indeed duplicate (this is the paranoid option of rmlint)       

If all these tests pass:

```bash
    rm -rf duplicate
    ln original duplicate
```













