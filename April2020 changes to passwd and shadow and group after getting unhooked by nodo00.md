# What happened

Our home folder got unhooked from nodo00 because I don't understand.       
Now, nodo00 will not be mounting /home/ska so our users' home folders need to be moved to /home/user     
        
Nodos of course continues to have /home/ska. 
Nodos inherited it users from nodo00, who now says that our homes are at /home/user instead of /home/ska/user so we would have problems.      
           
This inheritance works through 'nis' or 'yp' ? According to "/etc/nsswitch.conf":      
     
```
passwd:     files nis sss
shadow:     files nis sss
group:      files nis sss
```

nodos first looks for a user in its local files, then if the user is not found it'll look in nis, and then in sss (whatever that means).      


# The solution 

Instead of figuring out what nis is and how it works, I'll take advantage of the the hierarchy of nsswitch.conf.       
I 'll just add the relevant users and passwords and groups in the local files (which were empty of users).      
This way the system will ignore nis and jusr read the local file which will point to /home/ska/user instead of /home/user like nodo00 does
